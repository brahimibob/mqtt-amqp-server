package fr.erdf.acc_mqtt;

import java.net.URI;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.command.ActiveMQDestination;
 
public class Server1 implements MessageListener {
    private static int ackMode;
    private static String messageQueueName;
    private static String messageBrokerUrl;
 
    private Session session;
    private boolean transacted = false;
    private MessageProducer replyProducer;
    private MessageProtocol messageProtocol;
	private BrokerService broker;
 
    static {
        messageBrokerUrl = "tcp://localhost:61616";
        messageQueueName = "client.messages";
        ackMode = Session.AUTO_ACKNOWLEDGE;
    }
 
    public Server1() {
        try {
            //This message broker is embedded
            broker = BrokerFactory.createBroker(new URI("xbean:config/activemq.xml"));//new BrokerService();
         
			//  broker.setPersistent(false);
           // broker.setUseJmx(true);
          //  broker.addConnector(messageBrokerUrl);
          //  broker.setDestinationFactory(destinationFactory);
            broker.start();
        } catch (Exception e) {
            //Handle the exception appropriately
        }
 
        //Delegating the handling of messages to another class, instantiate it before setting up JMS so it
        //is ready to handle messages
        this.messageProtocol = new MessageProtocol();
        this.setupMessageQueueConsumer();
    }
 
    private void setupMessageQueueConsumer() {
        System.out.println(broker.getDefaultSocketURIString()); 
    	ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(broker.getDefaultSocketURIString());
        Connection connection;
        try {
            connection = (ActiveMQConnection)connectionFactory.createConnection();
            connection.start();
            this.session = connection.createSession(this.transacted, ackMode);
            Destination adminQueue = this.session.createQueue(messageQueueName);
           // Destination in = session.createTopic("ACC_IN_1");
            Destination out = session.createQueue("ACC_OUT_1");
            //Setup a message producer to respond to messages from clients, we will get the destination
            //to send to from the JMSReplyTo header field from a Message
            this.replyProducer = this.session.createProducer(null);
            this.replyProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
 
            //Set up a consumer to consume messages off of the admin queue
            MessageConsumer consumer = this.session.createConsumer(out);
            consumer.setMessageListener(this);
         
           Broker region = broker.getRegionBroker();
           try {
			ActiveMQDestination[] des = region.getDestinations();
			for (int i = 0; i < des.length; i++) {
				System.out.println(des[i]);
//				if(des[i].isQueue() && des[i].getPhysicalName().contains("ACC_IN") ){
//					
//					break;
//				}
				}
			//System.out.println(des); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
            
            
        } catch (JMSException e) {
            //Handle the exception appropriately
        }
    }
 
    public void onMessage(Message message) {
    	System.out.println(" ----------------------------------->\n "+message); 
//        try {
//            TextMessage response = this.session.createTextMessage();
//            if (message instanceof TextMessage) {
//                TextMessage txtMsg = (TextMessage) message;
//                String messageText = txtMsg.getText();
//                response.setText(this.messageProtocol.handleProtocolMessage(messageText));
//            }
// 
//            //Set the correlation ID from the received message to be the correlation id of the response message
//            //this lets the client identify which message this is a response to if it has more than
//            //one outstanding message to the server
//            response.setJMSCorrelationID(message.getJMSCorrelationID());
// 
//            //Send the response to the Destination specified by the JMSReplyTo field of the received message,
//            //this is presumably a temporary queue created by the client
//            this.replyProducer.send(message.getJMSReplyTo(), response);
//        } catch (JMSException e) {
//            //Handle the exception appropriately
//        }
    }
 
    public static void main(String[] args) {
        new Server();
    }
}