package fr.erdf.acc_mqtt.mqttclient;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Properties;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MqttPublishSample {

    public static void main(String[] args) {

        String topic        = "MQTT Examples boubakeur 1";
        String content      = "Message from MqttPublishSample";
        int qos             = 2;
        String broker       = "ssl://localhost:1883";
        String clientId     = "JavaSample";
        MemoryPersistence persistence = new MemoryPersistence();

        try {
        	
        	MqttClient sampleClient = new MqttClient("tcp://localhost:1884", MqttClient.generateClientId(), new MemoryPersistence());
        	 
        	SSLContext sslContext = SSLContext.getInstance("SSL");
        	TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        	KeyStore keyStore = readKeyStore();
        	trustManagerFactory.init(keyStore);
        	sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
        	 
        	MqttConnectOptions options = new MqttConnectOptions();
        	//options.setSocketFactory(sslContext.getSocketFactory());
        	 
        	sampleClient.connect(options);
        	
        	
        	
        	
//            MqttClient sampleClient = new MqttClient(broker, clientId, persistence);
//            MqttConnectOptions connOpts = new MqttConnectOptions();
//            connOpts.setCleanSession(true);
//            Properties props = new Properties();
//            
////            <property name="trustStore" value="/path/to/truststore.ts" />
////            <property name="trustStorePassword" value="password" />
////            <property name="keyStore" value="/path/to/keystore.ks" />
////            <property name="keyStorePassword" value="password" />
////            <property name="brokerURL" value="ssl://localhost:61616" />
////            <property name="userName" value="admin" /> 
////            <property name="password" value="admin" />
////            
//            props.setProperty("trustStore", "C:\\boubakeur\\apache-activemq-5.10.0\\conf\\broker.ts");
//            props.setProperty("trustStorePassword", "password");
//            props.setProperty("keyStore", "C:\\boubakeur\\apache-activemq-5.10.0\\conf\\client.ts");
//            props.setProperty("keyStorePassword", "password" );
////            props.setProperty(key, value);
//			
//            
//            
//            connOpts.setSSLProperties(props);
//            System.out.println("Connecting to broker: "+broker);
//            sampleClient.connect(connOpts);
        	
        	
            System.out.println("Connected");
            System.out.println("Publishing message: "+content);
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(qos);
            sampleClient.publish(topic, message);
            sampleClient.publish(topic, message);
            sampleClient.publish(topic, message);
            sampleClient.publish(topic, message);
            sampleClient.publish(topic, message);
            sampleClient.publish(topic, message);
            System.out.println("Message published");
            sampleClient.disconnect();
            System.out.println("Disconnected");
            
            sampleClient.connect(options);
            System.out.println("Connected");
            System.out.println("Publishing message: "+content);
            
            sampleClient.publish(topic, message);
            sampleClient.publish(topic, message);
            sampleClient.publish(topic, message);
            sampleClient.publish(topic, message);
            sampleClient.publish(topic, message);
            sampleClient.publish(topic, message);
            System.out.println("Message published");
            sampleClient.disconnect();
            System.out.println("Disconnected");
            
        	
            System.exit(0);
        } catch(Exception me) {
//            System.out.println("reason "+me.getReasonCode());
//            System.out.println("msg "+me.getMessage());
//            System.out.println("loc "+me.getLocalizedMessage());
//            System.out.println("cause "+me.getCause());
//            System.out.println("excep "+me);
            me.printStackTrace();
        }
    }

	private static KeyStore readKeyStore() throws Exception {
		
		
		String keystoreFilename = "C:\\boubakeur\\apache-activemq-5.14.3\\conf\\client.ts";

	    char[] password = "password".toCharArray();
	    String alias = "alias";

	    FileInputStream fIn = new FileInputStream(keystoreFilename);
	    KeyStore keystore = KeyStore.getInstance("JKS");

	    keystore.load(fIn, password);

	  //  Certificate cert = keystore.getCertificate(alias);

		
		
		return keystore;
	}
}