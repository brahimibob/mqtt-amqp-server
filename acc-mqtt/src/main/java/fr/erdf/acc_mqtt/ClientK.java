package fr.erdf.acc_mqtt;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Properties;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class ClientK {

	public void send() {

		String topic = "MQTT Examples boubakeur 1";
		String content = "Message from MqttPublishSample";
		int qos = 2;
		String broker = "ssl://localhost:1883";
		String clientId = "JavaSample";
		MemoryPersistence persistence = new MemoryPersistence();
		try {
			MqttClient sampleClient = new MqttClient("tcp://localhost:1884", MqttClient.generateClientId(),
					new MemoryPersistence());
			SSLContext sslContext = SSLContext.getInstance("SSL");
			TrustManagerFactory trustManagerFactory = TrustManagerFactory
					.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			KeyStore keyStore = readKeyStore();
			trustManagerFactory.init(keyStore);
			sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
			MqttConnectOptions options = new MqttConnectOptions();
			sampleClient.connect(options);
			System.out.println("Connected");
			System.out.println("Publishing message: " + content);
			MqttMessage message = new MqttMessage(content.getBytes());
			message.setQos(qos);
			sampleClient.publish(topic, message);
			System.out.println("Message published");
			
		} catch (Exception me) {
			me.printStackTrace();
		}
	}

	private static KeyStore readKeyStore() throws Exception {

		String keystoreFilename = "C:\\boubakeur\\apache-activemq-5.10.0\\conf\\client.ts";

		char[] password = "password".toCharArray();
		String alias = "alias";

		FileInputStream fIn = new FileInputStream(keystoreFilename);
		KeyStore keystore = KeyStore.getInstance("JKS");

		keystore.load(fIn, password);

		// Certificate cert = keystore.getCertificate(alias);

		return keystore;
	}
}