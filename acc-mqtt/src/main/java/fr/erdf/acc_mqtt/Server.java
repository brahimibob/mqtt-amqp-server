package fr.erdf.acc_mqtt;

import java.net.URI;
import java.net.URISyntaxException;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.command.ActiveMQDestination;

/**
 * Hello world!
 *
 */
public class Server implements MessageListener {
	// static BrokerService broker = new BrokerService();
	static BrokerService brokerService;

	public static void main(String[] args) {
		new Server();
	}

	Server() {
		try {
			brokerService = BrokerFactory.createBroker(new URI("xbean:config/activemq.xml"));
			brokerService.start();
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
					brokerService.getDefaultSocketURIString());
			Connection connection = connectionFactory.createConnection();
			connection.start();
			
			boolean transacted = false;
			Session session = connection.createSession(transacted, Session.AUTO_ACKNOWLEDGE);
			// Destination adminQueue = session.createQueue("ACC_IN_1");
			Destination out = session.createQueue("ACC_OUT_1");

			// Setup a message producer to respond to messages from clients, we
			// will get the destination
			// to send to from the JMSReplyTo header field from a Message
			MessageProducer replyProducer = session.createProducer(null);
			replyProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

			// Set up a consumer to consume messages off of the admin queue
			MessageConsumer consumer = session.createConsumer(out);
			consumer.setMessageListener(this);

			// *************************************************************************************************//

			Broker region = brokerService.getRegionBroker();
			try {
				ActiveMQDestination[] des = region.getDestinations();
				for (int i = 0; i < des.length; i++) {
					System.out.println(des[i] + " --  ");
					// if(des[i].isQueue() &&
					// des[i].getPhysicalName().contains("ACC_IN") ){
					//
					// break;
					// }
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// *************************************************************************************************//
//			 while (true) {
//			 try {
//			 Thread.sleep(1500000);
//			 } catch (InterruptedException e) {
//			 e.printStackTrace();
//			 }
//			 }

		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// System.out.println( "Hello World!" );
	}

	public void onMessage(Message paramMessage) {
		// TODO Auto-generated method stub
		ClientK clientK=new ClientK();
		try {
			clientK.send();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		System.out.println("message" + paramMessage);

	}
}
